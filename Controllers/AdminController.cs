﻿using Microsoft.AspNetCore.Mvc;
using VitacoreTestApp.Data.Entities;
using VitacoreTestApp.Data.Repositories;

namespace VitacoreTestApp.Controllers
{
    [Route("api/admin/")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly ICommonRepository _repository;

        public AdminController(ICommonRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("doctors")]
        public IEnumerable<Doctor> GetDoctors()
        {
            return _repository.GetDoctors();
        }

        [HttpPost]
        [Route("adddoctor")]
        public IResult AddDoctor(string fullName)
        {
            try
            {
                var doctorEntity = new Doctor
                {
                    FullName = fullName,
                };
                _repository.AddDoctor(doctorEntity);
                return Results.Ok();
            }
            catch (Exception ex)
            {
                return Results.Problem(ex.Message);
            }
        }

        [HttpPut]
        [Route("editdoctor/{id}")]
        public IResult EditDoctor(int id, string newName)
        {
            var entity = _repository.GetDoctors().FirstOrDefault(x => x.Id == id);
            if (entity == null) 
            {
                return Results.NotFound($"There is no any doctor with id {id}");
            }

            entity.FullName = newName;
            _repository.Save();
            return Results.Ok();
        }

        [HttpDelete]
        [Route("deletedoctor/{id}")]
        public IResult DeleteDoctor(int id)
        {
            var entity = _repository.GetDoctors().FirstOrDefault(x => x.Id == id);
            if (entity == null)
            {
                return Results.NotFound($"There is no any doctor with id {id}");
            }

            _repository.DeleteDoctor(id);
            return Results.Ok();
        }

        [HttpPut]
        [Route("bindservice")]
        public IResult BindService(int doctorId, int serviceId)
        {
            var doctor = _repository.GetDoctors().FirstOrDefault(d => d.Id == doctorId);
            if (doctor == null)
            {
                return Results.NotFound($"There is no any doctor with id {doctorId}");
            }

            var service = _repository.GetServices().FirstOrDefault(s => s.Id == serviceId);
            if (service == null) 
            {
                return Results.NotFound($"There is no any service with id {serviceId}");
            }

            doctor.Services ??= new List<Service>();
            doctor.Services.Add(service);
            _repository.Save();
            return Results.Ok();
        }

        [HttpPut]
        [Route("unbindservice")]
        public IResult UnbindService(int doctorId, int serviceId)
        {
            var doctor = _repository.GetDoctors().FirstOrDefault(d => d.Id == doctorId);
            if (doctor == null)
            {
                return Results.NotFound($"There is no any doctor with id {doctorId}");
            }

            var service = _repository.GetServices().FirstOrDefault(s => s.Id == serviceId);
            if (service == null)
            {
                return Results.NotFound($"There is no any service with id {serviceId}");
            }

            if (doctor.Services.Any(s => s.Id == serviceId))
            {
                doctor.Services.Remove(service);
                _repository.Save();
                return Results.Ok();
            }
            return Results.NotFound($"There is no service with id {serviceId} in doctor with id {doctorId}");
        }

        #region Services
        [HttpGet]
        [Route("services")]
        public IEnumerable<Service> GetServices() 
        {
            return _repository.GetServices();
        }

        [HttpPost]
        [Route("addservice")]
        public IResult AddService(Service service)
        {
            try
            {
                _repository.AddService(service);
                return Results.Ok();
            }
            catch (Exception ex) 
            {
                return Results.Problem(ex.Message);
            }
        }

        [HttpPut]
        [Route("editservice")]
        public IResult EditService(Service service)
        {
            var entity = _repository.GetServices().FirstOrDefault(x => x.Id == service.Id);
            if (entity == null)
            {
                return Results.NotFound($"There is no any service with id {service.Id}");
            }

            entity.Title = service.Title;
            entity.Description = service.Description;
            _repository.Save();
            return Results.Ok();
        }

        [HttpDelete]
        [Route("deleteservice/{id}")]
        public IResult DeleteService(int id)
        {
            var entity = _repository.GetServices().FirstOrDefault(x => x.Id == id);
            if (entity == null)
            {
                return Results.NotFound($"There is no any service with id {id}");
            }

            _repository.DeleteService(id);
            return Results.Ok();
        }
        #endregion
    }
}
