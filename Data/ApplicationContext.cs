﻿using Microsoft.EntityFrameworkCore;
using VitacoreTestApp.Data.Entities;

namespace VitacoreTestApp.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<AnimalOwner> Owners { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Vaccine> Vaccines { get; set; }
    }
}
