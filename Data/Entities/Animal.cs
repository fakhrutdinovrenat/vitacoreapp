﻿namespace VitacoreTestApp.Data.Entities
{
    public class Animal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Species { get; set; }
        public string Breed { get; set; }
        public int OwnerId { get; set; }
        public AnimalOwner Owner { get; set; }
        public List<Vaccine> Vaccines { get; set; }
    }
}
