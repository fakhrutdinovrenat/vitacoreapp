﻿namespace VitacoreTestApp.Data.Entities
{
    public class AnimalOwner
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Doctor? Doctor { get; set; }
        public List<Animal> Animals { get; set; }
    }
}
