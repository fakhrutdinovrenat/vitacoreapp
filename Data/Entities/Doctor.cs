﻿namespace VitacoreTestApp.Data.Entities
{
    public class Doctor
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public List<Service>? Services { get; set; }
        public List<AnimalOwner>? Clients { get; set; }
    }
}
