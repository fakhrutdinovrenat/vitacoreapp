﻿using System.Text.Json.Serialization;

namespace VitacoreTestApp.Data.Entities
{
    public class Vaccine
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public List<Animal>? Animals { get; } 
    }
}
