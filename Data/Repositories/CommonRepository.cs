﻿using Microsoft.EntityFrameworkCore;
using VitacoreTestApp.Data.Entities;

namespace VitacoreTestApp.Data.Repositories
{
    public class CommonRepository : ICommonRepository
    {
        private readonly ApplicationContext _context;

        public CommonRepository(ApplicationContext context) 
        {
            _context = context;
        }
        public IEnumerable<Doctor> GetDoctors()
        {
            return _context.Doctors.Include(d => d.Clients).Include(d => d.Services);
        }

        public void AddDoctor(Doctor doctor)
        {
            _context.Doctors.Add(doctor);
            Save();
        }

        public void DeleteDoctor(int id)
        {
            var entity = _context.Doctors.Where(d => d.Id == id).FirstOrDefault();
            _context.Doctors.Remove(entity);
            Save();
        }

        public IEnumerable<Service> GetServices()
        {
            return _context.Services;
        }

        public void AddService(Service service)
        {
            _context.Services.Add(service);
            Save();
        }

        public void DeleteService(int id)
        {
            var entity = _context.Services.Where(s => s.Id == id).FirstOrDefault();
            _context.Services.Remove(entity);
            Save();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
