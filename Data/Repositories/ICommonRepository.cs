﻿using VitacoreTestApp.Data.Entities;

namespace VitacoreTestApp.Data.Repositories
{
    public interface ICommonRepository
    {
        #region Doctor entity
        public IEnumerable<Doctor> GetDoctors();
        public void AddDoctor(Doctor doctor);
        public void DeleteDoctor(int id);
        #endregion

        #region Service entity
        public IEnumerable<Service> GetServices();
        public void AddService(Service service);
        public void DeleteService(int id);
        #endregion
        public void Save();
    }
}
