﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VitacoreTestApp.Migrations
{
    public partial class AnimalVaccineConnection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vaccines_Animals_AnimalId",
                table: "Vaccines");

            migrationBuilder.DropIndex(
                name: "IX_Vaccines_AnimalId",
                table: "Vaccines");

            migrationBuilder.DropColumn(
                name: "AnimalId",
                table: "Vaccines");

            migrationBuilder.CreateTable(
                name: "AnimalVaccine",
                columns: table => new
                {
                    AnimalsId = table.Column<int>(type: "int", nullable: false),
                    VaccinesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnimalVaccine", x => new { x.AnimalsId, x.VaccinesId });
                    table.ForeignKey(
                        name: "FK_AnimalVaccine_Animals_AnimalsId",
                        column: x => x.AnimalsId,
                        principalTable: "Animals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AnimalVaccine_Vaccines_VaccinesId",
                        column: x => x.VaccinesId,
                        principalTable: "Vaccines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnimalVaccine_VaccinesId",
                table: "AnimalVaccine",
                column: "VaccinesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnimalVaccine");

            migrationBuilder.AddColumn<int>(
                name: "AnimalId",
                table: "Vaccines",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vaccines_AnimalId",
                table: "Vaccines",
                column: "AnimalId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vaccines_Animals_AnimalId",
                table: "Vaccines",
                column: "AnimalId",
                principalTable: "Animals",
                principalColumn: "Id");
        }
    }
}
