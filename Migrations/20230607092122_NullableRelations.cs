﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VitacoreTestApp.Migrations
{
    public partial class NullableRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Owners_Doctors_DoctorId",
                table: "Owners");

            migrationBuilder.AlterColumn<int>(
                name: "DoctorId",
                table: "Owners",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Owners_Doctors_DoctorId",
                table: "Owners",
                column: "DoctorId",
                principalTable: "Doctors",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Owners_Doctors_DoctorId",
                table: "Owners");

            migrationBuilder.AlterColumn<int>(
                name: "DoctorId",
                table: "Owners",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Owners_Doctors_DoctorId",
                table: "Owners",
                column: "DoctorId",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
