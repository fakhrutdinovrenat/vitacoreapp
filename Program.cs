using Microsoft.EntityFrameworkCore;
using VitacoreTestApp.Data;
using VitacoreTestApp.Data.Repositories;

namespace VitacoreTestApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            string dbConnection = builder.Configuration.GetValue<string>("ConnectionStrings:sqlConnection");
            builder.Services.AddDbContext<ApplicationContext>(opt => opt.UseSqlServer(dbConnection));
            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddScoped<ICommonRepository, CommonRepository>();

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}